# Оружейный магазин

# Описание предметной области
Система "Оружейный магазин" предназначена для продажи товаров в оружейном магазине. Также система предназначена для внесения новой информации о товарах, клиентах, сотрудниках; просмотр и изменение данных. Основная цель приложения - упростить и автоматизировать процессы, связанные с работой сотрудников.Полная информация представлена в техническом задании.

# ERD-диаграмма

![image](https://gitlab.com/Strawb_17/weaponshop/-/raw/main/Images/2.png)

# Use-case диаграмма
![image](https://gitlab.com/4-9-14/weaponshopkonev/-/raw/main/1.png)

# Figma
https://www.figma.com/file/Wyfb5GjHVBXviK7OujEIyQ?type=design


# Диаграмма последовательностей
![image](https://gitlab.com/Strawb_17/weaponshop/-/raw/main/Images/5.png)


# Диаграмма классов
![image](https://gitlab.com/Strawb_17/weaponshop/-/raw/main/Images/3.png)

# Диаграмма активности
![image](https://gitlab.com/Strawb_17/weaponshop/-/raw/main/Images/4.png)
