﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.ClassHelper;
using WeaponShop.DB;
using static WeaponShop.ClassHelper.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        List<Product> products = new List<Product>();
        List<string> sortList = new List<string>()
        {
            "По умолчанию",
            "По цене"
        };
        
        public MenuWindow()
        {
            InitializeComponent();
            
            if (EmployeeDataClass.Employee.IdSpecialization != 2)
            {
                btnAdd.Visibility = Visibility.Collapsed;
                btnEdit.Visibility = Visibility.Collapsed;
            }
            cmbSort.ItemsSource = sortList;
            cmbSort.SelectedIndex = 0;
            GetSortProd();
        }

        private void GetSortProd()
        {
           products = Context.Product.ToList();
           products = products.Where(i => i.ProductTitle.Contains(tbSearch.Text)).ToList();
            switch (cmbSort.SelectedIndex)
            { 
                case 0:
                    products = products.OrderBy(i => i.IdProduct).ToList();
                    break;
                case 1:
                    products = products.OrderBy(i =>i.Cost).ToList();
                    break;
                default:
                    break;
            }
            LvProductList.ItemsSource = products;
        }
        private void GetProduct()
        {
            List<Product> productsList = new List<Product>();
            productsList = Context.Product.ToList();
            LvProductList.ItemsSource = productsList;
        }

        private void btnCart_Click(object sender, RoutedEventArgs e)
        {
            CartWindow cartWindow = new CartWindow();
            this.Hide();
            cartWindow.ShowDialog();
            this.Show();
        }

        private void btnAddtoCart_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            bool seek = true;
            if (button == null)
            {
                return;
            }

            var selectedProduct = button.DataContext as Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < CartClass.Products.Count; i++)
                {
                    if (CartClass.Products[i] == selectedProduct)
                    {
                        CartClass.Products[i].Quantity++;
                        seek = false;
                    }
                }
                if (seek)
                {
                    CartClass.Products.Add(selectedProduct);
                }
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            MenuChangeWindow menuChangeWindow = new MenuChangeWindow();
            this.Hide();
            menuChangeWindow.ShowDialog();
            this.Show();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (LvProductList.SelectedItem is Product)
            {
                var product = LvProductList.SelectedItem as Product;
                MenuChangeWindow menuChangeWindow = new MenuChangeWindow(product);
                menuChangeWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Выберите запись, которую нужно изменить!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            NavigateWindow navigateWindow = new NavigateWindow();
            navigateWindow.Show();
            this.Close();
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            GetSortProd();
        }

        private void cmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetSortProd();
        }
    }
}
