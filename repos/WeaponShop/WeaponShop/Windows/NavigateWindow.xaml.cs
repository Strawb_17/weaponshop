﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для NavigateWindow.xaml
    /// </summary>
    public partial class NavigateWindow : Window
    {
        public NavigateWindow()
        {
            InitializeComponent();
        }

        private void BtnClientList_Click(object sender, RoutedEventArgs e)
        {
            ClientListWindow clientListWindow = new ClientListWindow();
            clientListWindow.Show();
            this.Close();
        }

        private void BtnProductList_Click(object sender, RoutedEventArgs e)
        {
            MenuWindow menuWindow = new MenuWindow();
            menuWindow.Show();
            this.Close();
        }

        private void BtnEmployeeList_Click(object sender, RoutedEventArgs e)
        {
            EmployeeListWindow employeeListWindow = new EmployeeListWindow();   
            employeeListWindow.Show();
            this.Close();
        }

        private void BtnTransactionList_Click(object sender, RoutedEventArgs e)
        {
            TransactionListWindow transactionListWindow = new TransactionListWindow();
            transactionListWindow.Show();
            this.Close();
        }
    }
}
