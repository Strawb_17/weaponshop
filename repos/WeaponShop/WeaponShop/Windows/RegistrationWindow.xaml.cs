﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WeaponShop.ClassHelper.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
            CbSpec.ItemsSource = Context.Specialization.ToList();
            CbSpec.SelectedIndex = 0;
            CbSpec.DisplayMemberPath = "SpecializationTitle";
            CbGen.ItemsSource = Context.Gender.ToList();
            CbGen.SelectedIndex = 0;
            CbGen.DisplayMemberPath = "GenderTitle";
        }

        private void BtnSignIn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TbLastName.Text) ||
                string.IsNullOrWhiteSpace(TbFirstName.Text)||
                string.IsNullOrWhiteSpace(DpBirthday.Text) ||
                string.IsNullOrWhiteSpace(CbSpec.Text)||
                string.IsNullOrWhiteSpace(CbGen.Text)||
                string.IsNullOrWhiteSpace(TbPhoneNum.Text) ||
                string.IsNullOrWhiteSpace(TbLogin.Text) ||
                string.IsNullOrWhiteSpace(PbPassword.Password))
            {
                MessageBox.Show("Все поля должны быть заполнены!", "Ошибка");
                return;
            }
            var authUser = Context.Employee.ToList()
                .Where(i => i.Login == TbLogin.Text).FirstOrDefault();
            if (authUser != null)
            {
                MessageBox.Show("Такой логин уже занят", "Ошибка");
                return;
            }

            DB.Employee employee = new DB.Employee();
            employee.LastName = TbLastName.Text;
            employee.FirstName = TbFirstName.Text;
            employee.Birthday = DpBirthday.SelectedDate.Value;
            employee.IdSpecialization = (CbSpec.SelectedItem as DB.Specialization).IdSpecialization;
            employee.IdGender = (CbGen.SelectedItem as DB.Gender).IdGender;
            employee.PhoneNumber = TbPhoneNum.Text;
            employee.Login = TbLogin.Text;
            employee.Password = PbPassword.Password;

            Context.Employee.Add(employee);
            Context.SaveChanges();

            NavigateWindow navigateWindow = new NavigateWindow();
            navigateWindow.Show();
            this.Close();
        }
    }
}
