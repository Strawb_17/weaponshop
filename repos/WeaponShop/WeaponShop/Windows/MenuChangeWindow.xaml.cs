﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using WeaponShop.ClassHelper;
using static WeaponShop.ClassHelper.EFClass;
using WeaponShop.Windows;
using WeaponShop.DB;
using static System.Net.Mime.MediaTypeNames;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для MenuChangeWindow.xaml
    /// </summary>
    public partial class MenuChangeWindow : Window
    {
        private bool isMenuChange = false;
        private Product editproduct;
        private string Image = null;
        byte[] imgtemp = null;

        public MenuChangeWindow()
        {
            InitializeComponent();
            CMBTypeProduct.ItemsSource = EFClass.Context.CategoryProduct.ToList();
            CMBTypeProduct.SelectedIndex = 0;
            CMBTypeProduct.DisplayMemberPath = "CategoryTitle";

            CMBLicense.ItemsSource = EFClass.Context.License.ToList();
            CMBLicense.SelectedIndex = 0;
            CMBLicense.DisplayMemberPath = "LicenseTitle";
        }
        public MenuChangeWindow(Product product)
        {
            InitializeComponent();
            TxtTitle.Text = "Изменение продукта";
            BtnAdd.Content = "Обновить";

            CMBTypeProduct.ItemsSource = EFClass.Context.CategoryProduct.ToList();
            CMBTypeProduct.SelectedIndex = 0;
            CMBTypeProduct.DisplayMemberPath = "CategoryTitle";

            CMBLicense.ItemsSource = EFClass.Context.License.ToList();
            CMBLicense.SelectedIndex = 0;
            CMBLicense.DisplayMemberPath = "LicenseTitle";

            TbNameProduct.Text = product.ProductTitle.ToString();
            TbDisc.Text = product.Description.ToString();
            CMBTypeProduct.SelectedItem = EFClass.Context.CategoryProduct.Where(i => i.IdCategoryProduct == product.IdCategoryProduct).FirstOrDefault();
            CMBLicense.SelectedItem = EFClass.Context.License.Where(i => i.IdLicense == product.IdLicense).FirstOrDefault();
            TbPrice.Text = product.Cost.ToString();
            try
            {
                MemoryStream stream = new MemoryStream(Convert.ToInt32(product.Image));
                ImgProduct.Source = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            catch
            {
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri(@"\Resourses\Picture\notImage.png", UriKind.Relative);
                bi3.EndInit();
                ImgProduct.Source = bi3;
            }               
            isMenuChange = true;
            editproduct = product;
        }

            private void BtnAdd_Click(object sender, RoutedEventArgs e)
            {
            //Проверка на пустые поля при добавлении
            if (string.IsNullOrWhiteSpace(TbNameProduct.Text) ||
                string.IsNullOrWhiteSpace(CMBTypeProduct.Text) ||
                string.IsNullOrWhiteSpace(CMBLicense.Text) ||
                string.IsNullOrWhiteSpace(TbPrice.Text))
            {
                MessageBox.Show("Не все поля заполнены!", "Ошибка");
                return;
            }

            //Проверка на существующий продукт
            var titleProd = EFClass.Context.Product.ToList()
                .Where(i => i.ProductTitle == TbNameProduct.Text).FirstOrDefault();
            if (titleProd != null)
            {
                MessageBox.Show("Такой продукт уже существует", "Ошибка");
                return;
            }    
            if (isMenuChange)
            {
                editproduct.ProductTitle = TbNameProduct.Text;
                editproduct.Description = TbDisc.Text;
                editproduct.Cost = Convert.ToDecimal(TbPrice.Text);
                editproduct.IdCategoryProduct = (CMBTypeProduct.SelectedItem as CategoryProduct).IdCategoryProduct;
                editproduct.IdLicense = (CMBLicense.SelectedItem as License).IdLicense;
                if (Image != null)
                {
                    editproduct.Image = File.ReadAllBytes(Image);
                }
                EFClass.Context.SaveChanges();
                MessageBox.Show("Товар успешно обновлен!", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();
                this.Close();
            }
            else
            {
                //Заполнение экземпляра класса данными
                Product product = new Product();
                product.ProductTitle = TbNameProduct.Text;
                product.Description = TbDisc.Text;
                product.Cost = Convert.ToDecimal(TbPrice.Text);
                product.IdCategoryProduct = (CMBTypeProduct.SelectedItem as CategoryProduct).IdCategoryProduct;
                product.IdLicense = (CMBLicense.SelectedItem as License).IdLicense;
                if (Image != null)
                {
                    product.Image = File.ReadAllBytes(Image);
                }
                EFClass.Context.Product.Add(product);
                EFClass.Context.SaveChanges();
                MessageBox.Show("Товар успешно добавлен", "Успех!", MessageBoxButton.OK, MessageBoxImage.Information);
                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();
                this.Close();
            }
        }   
    

        private void BtnChooseImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Filter = "Image Files|*.jpg;*.jpeg;*.png";
            if (openFileDialog.ShowDialog() == true)
            {
                ImgProduct.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                Image = openFileDialog.FileName;
            }
        }
    }
}
