﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.ClassHelper;
using WeaponShop.DB;
using static WeaponShop.ClassHelper.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для CartWindow.xaml
    /// </summary>
    public partial class CartWindow : Window
    {
        public CartWindow()
        {
            InitializeComponent();
            GetListProduct();
        }

        private void GetListProduct()
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>(CartClass.Products);
            LvCartList.ItemsSource = products;
            GetPrice(products);
        }

        private void GetPrice(ObservableCollection<Product> products)
        {
            decimal price = 0;
            foreach (var item in CartClass.Products)
            {
                price += item.Cost * item.Quantity;
            }
            price = Math.Round(price, 2);
            TbPrice.Text = price.ToString();
        }


        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBuy_Click(object sender, RoutedEventArgs e)
        {
            Sale sale = new Sale();
            sale.IdClient = 8;
            sale.IdEmployee = EmployeeDataClass.Employee.IdEmployee;   
            sale.SaleDate = DateTime.Now;
            if (sale != null)
            {
                EFClass.Context.Sale.Add(sale);
                EFClass.Context.SaveChanges();
            }

            foreach (var item in CartClass.Products)
            {
                ProductSale productSale = new ProductSale();
                productSale.IdSale = EFClass.Context.Sale.ToList().LastOrDefault().IdSale;
                productSale.IdProduct = item.IdProduct;
                productSale.Quantity = (short)item.Quantity;
                
                productSale.FinalPrice = item.Cost * item.Quantity;

                EFClass.Context.ProductSale.Add(productSale);
                EFClass.Context.SaveChanges();
            }
            MessageBox.Show("Покупка успешно осуществлена!", "Успех!");
            CartClass.Products.Clear();
            this.Close();
        }

        private void btnRemoveFromCart_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            var selectedProduct = button.DataContext as Product;

            if (selectedProduct != null)
            {
                selectedProduct.Quantity = 1;
                CartClass.Products.Remove(selectedProduct);

            }
            GetListProduct();
        }

        private void btnMinus_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            Product selectedProduct = button.DataContext as Product;

            if (selectedProduct != null)
            {
                if (selectedProduct.Quantity == 1 || selectedProduct.Quantity == 0)
                {
                    CartClass.Products.Remove(selectedProduct);
                }
                else
                {
                    selectedProduct.Quantity--;
                    int a = CartClass.Products.IndexOf(selectedProduct);
                    CartClass.Products.Remove(selectedProduct);
                    CartClass.Products.Insert(a, selectedProduct);
                }
            }
            GetListProduct();
        }

        private void btnPlus_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }

            Product selectedProduct = button.DataContext as Product;

            if (selectedProduct != null)
            {
                selectedProduct.Quantity++;
                int a = CartClass.Products.IndexOf(selectedProduct);
                CartClass.Products.Remove(selectedProduct);
                CartClass.Products.Insert(a, selectedProduct);
            }
            GetListProduct();
        }
    }
}
